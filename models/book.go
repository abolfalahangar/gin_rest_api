package models

import (
	"gorm.io/gorm"
)

type Book struct {
	gorm.Model
	ID     uint   `json:"id" gorm:"primary_key"`
	Title  string `json:"title" gorm:"title"`
	Author string `json:"author" gorm:"author"`
}
type CreateBookInput struct {
	gorm.Model
	Title  string `json:"title" gorm:"title" binding:"required"`
	Author string `json:"author" gorm:"author" binding:"required"`
}
type UpdateBookInput struct {
	gorm.Model
	Title  string `json:"title" gorm:"title"`
	Author string `json:"author" gorm:"author"`
}

func GetBookById(id int) (bool, error) {
	var book Book
	err := db.Select("id").Where("id = ? AND deleted_on = ? ", id, 0).First(&book).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if book.ID > 0 {
		return true, nil
	}

	return false, nil
}
