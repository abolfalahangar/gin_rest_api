package models

import (
	"fmt"
	"os"

	"gorm.io/driver/postgres" // using postgres sql
	"gorm.io/gorm"
)

var db *gorm.DB
var err error

func SetupModels() *gorm.DB {
	_db := os.Getenv("POSTGRES_DB")
	user := os.Getenv("POSTGRES_USER")
	password := os.Getenv("POSTGRES_PASSWORD")
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=disable TimeZone=Asia/Shanghai", host, user, password, _db, port)
	fmt.Printf("postgres.Open(dsn) : %v", postgres.Open(dsn))
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}
	db.AutoMigrate(&Book{}, &User{})
	// Initialise value
	m := Book{Author: "author1", Title: "title1"}
	db.Create(&m)
	return db
}
