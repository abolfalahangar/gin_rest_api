package models

import (
	"project/forms"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID         uint   `json:"id" gorm:"primary_key"`
	Name       string `json:"name" gorm:"name"`
	Email      string `json:"email" gorm:"email"`
	Password   string `json:"password" gorm:"password"`
	IsVerified bool   `json:"is_verified" gorm:"is_verified"`
}

type UserModel struct{}

func (u *UserModel) GetUserByEmail(email string) (user User, err error) {
	// Connect to the user collection
	err = db.Model(&user).Where("email = ?", email).First(&user).Error
	return user, err
}

func (u *UserModel) SignUp(data forms.SignupUserCommand) (err error) {

	err = db.Table("users").Create(&data).Error
	return err
}

func (u *UserModel) Login(data forms.LoginUserCommand) (err error) {

	err = db.Table("users").Create(&data).Error
	return err
}

func (u *UserModel) GetAllUsers() (user []User, err error) {

	err = db.Model(&user).Error

	return user, err
}
