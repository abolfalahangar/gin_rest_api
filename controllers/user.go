package controllers

import (
	"net/http"
	"project/forms"
	"project/models"
	"project/services"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var userModel = new(models.UserModel)

type UserController struct {
}

func (u *UserController) Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var user forms.LoginUserCommand
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	result, _ := userModel.GetUserByEmail(user.Email)
	if result.Email != "" {
		c.JSON(403, gin.H{"message": "Email is already in use"})
		c.Abort()
		return
	}

	db.Find(&user)
	c.JSON(http.StatusOK, gin.H{"data": user})
}

func (u *UserController) SignUp(c *gin.Context) {

	var data forms.SignupUserCommand
	if c.BindJSON(&data) != nil {
		// specified response
		c.JSON(406, gin.H{"message": "Provide relevant fields"})
		// abort the request
		c.Abort()
		// return nothing
		return
	}
	result, _ := userModel.GetUserByEmail(data.Email)
	if result.Email != "" {
		c.JSON(403, gin.H{"message": "Email is already in use"})
		c.Abort()
		return
	}

	err := userModel.SignUp(data)
	if err != nil {
		c.JSON(400, gin.H{"message": "Problem creating an account"})
		c.Abort()
		return
	}

	c.JSON(201, gin.H{"message": "New user account registered"})

}

func (u *UserController) RefreshToken(c *gin.Context) {
	// Get refresh token from header
	refreshToken := c.Request.Header["Refreshtoken"]

	// Check if refresh token was provided
	if refreshToken == nil {
		c.JSON(403, gin.H{"message": "No refresh token provided"})
		c.Abort()
		return
	}

	// Decode token to get data
	email, err := services.DecodeRefreshToken(refreshToken[0])

	if err != nil {
		c.JSON(500, gin.H{"message": "Problem refreshing your session"})
		c.Abort()
		return
	}

	// Create new token
	accessToken, _refreshToken, _err := services.GenerateToken(email)

	if _err != nil {
		c.JSON(500, gin.H{"message": "Problem creating new session"})
		c.Abort()
		return
	}

	c.JSON(200, gin.H{"message": "Log in success", "token": accessToken, "refresh_token": _refreshToken})
}

// Get /users
func (u *UserController) GetAllUsers(c *gin.Context) {
	result, _ := userModel.GetAllUsers()

	c.JSON(200, gin.H{"message": result})

}
