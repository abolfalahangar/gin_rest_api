package main

import (
	"log"
	"project/controllers"
	"project/models"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Printf("No .env file found")
	}

}

func main() {
	// gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.Use(gin.Logger())
	db := models.SetupModels() // new

	r.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	})
	c := new(controllers.BookController)
	u := new(controllers.UserController)
	r.GET("/users", u.GetAllUsers)
	r.GET("/login", u.Login)
	r.GET("/singUp", u.SignUp)
	// r.POST("/user", u.AddUser)
	r.GET("/books", c.FindBooks)
	r.POST("/books", c.CreateBook)       // create
	r.GET("/books/:id", c.FindBook)      // find by id
	r.PATCH("/books/:id", c.UpdateBook)  // update by id
	r.DELETE("/books/:id", c.DeleteBook) // delete by id
	r.Run()

}
